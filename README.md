# Kwantumkwestie

[kwantumkwestie.nl](https://kwantumkwestie.nl).

Een website die, wanneer je twee opties geeft, bepaalt welke je moet doen. De
keuze wordt gebaseerd op een Quantum Random Number Generator 
(van [qrng.anu.edu.au](https://qrng.anu.edu.au/)).


Dit betekent dat, volgens de
[Veel-werelden-interpretatie](https://nl.wikipedia.org/wiki/Veel-werelden-interpretatie)
van kwantummechanica, er een universum zal zijn waarin je optie 1 doet, en een
universum zal zijn waar je optie 2 zal doen, aangezien jij je keuze laat
afhangen van een kwantummeting.

Dit is een fork van
[slitdecision](https://gitlab.com/RensOliemans/slitdecision), dit leek mij de
meest eenvoudige manier om verschillende talen te hebben en alsnog GitLab pages
te kunnen gebruiken.
